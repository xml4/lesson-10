package com.feeko256.taskl10

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.EditText

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    public fun acceptInfoButtonClick(view: View) {
        val nameText = findViewById<EditText>(R.id.nameText)
        val lastNameText = findViewById<EditText>(R.id.lastNameText)
        val middleNameText = findViewById<EditText>(R.id.middleNameText)
        val ageText = findViewById<EditText>(R.id.ageText)
        val hobby = findViewById<EditText>(R.id.hobbyText)

        val user = UserData(
            firstName = nameText.text.toString(),
            lastName = lastNameText.text.toString(),
            middleName = middleNameText.text.toString(),
            age = ageText.text.toString().toInt(),
            hobby = hobby.text.toString(),
        )
        val intent = Intent(this, InfoActivity::class.java)
        intent.putExtra("userData", user)
        startActivity(intent)
    }
}