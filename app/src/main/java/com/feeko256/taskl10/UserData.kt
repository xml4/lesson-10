package com.feeko256.taskl10

import android.os.Parcelable
import kotlinx.parcelize.Parcelize


@Parcelize
data class UserData(
    val firstName: String,
    val lastName: String,
    val middleName: String,
    val age: Int,
    val hobby: String,
) : Parcelable